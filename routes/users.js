import Express from "express";
import { createUser } from "../controllers/users";
//import middleware from "../middleware";
import { isDataValidate } from "../validator/userValidator";

const route = Express.Router();

route.post("/createUser", [isDataValidate], createUser);

export default route;
