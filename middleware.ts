import bluebird from "bluebird";
import { Request, Response, NextFunction } from "express";
export const validateReq = (
  validators: { (v: Request): { message: string; field: string } }[]
) => async (req: Request, res: Response, next: NextFunction) => {
  const validationErrors: { message: string; field: string }[] = [];
  const results = await bluebird.map(validators, async (validator) => {
    if (typeof validator !== "function") {
      throw "Validator is not a function";
    }
    return await validator(req);
  });

  results.forEach((result) => {
    if (result) {
      validationErrors.push({
        message: result.message,
        field: result.field,
      });
    }
  });

  if (validationErrors.length) {
    return res.status(400).send({
      message:
        validationErrors && validationErrors.length
          ? validationErrors[0].message
          : "Validation failed",
      reasons: validationErrors,
    });
  }
  next();
};

export default validateReq;
