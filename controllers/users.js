import usersModel from "../models/usersModel";
import { genHash } from "../helpers/functions";
export const createUser = async (req, res) => {
  try {
    const {
      email,
      password,
      firstName,
      lastName,
      phoneNumber,
      gender,
      address,
      biography,
    } = req.body;
    const saltRounds = 10;
    const hash = await genHash(password, saltRounds);

    const result = await usersModel.create({
      email,
      password: hash,
      firstName,
      lastName,
      phoneNumber,
      gender,
      address,
      biography,
    });
    res.status(200).send({
      status: 1,
      message: "SuccessFully created user):-",
      response: result,
    });
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: 0,
      message: "Bad Request at controller",
    });
  }
};
