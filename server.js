import express from "express";
import path from "path";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import fs from "fs";
import util from "util";
import csv from "csv-parse";

// import routes
import createUser from "./routes/users";

// define  app using express
const app = express();

//for csv file import code
fs.createReadStream("users.csv")
  .pipe(csv())
  .on("data", (row) => {
    console.log(row);
  })
  .on("end", () => {
    console.log("CSV file successfully processed");
  });
//end code of import csv file

// allow-cors
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
// configure app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));
process.title = "server";
// set the port
const port = process.env.PORT || 3000;

// connection with database
let mongoDB = "mongodb://127.0.0.1/incipientinfo";

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
//end of connection code

app.use("/api", createUser);

//start error log
let log_file = fs.createWriteStream("debug.log", { flags: "a" });
let log_stdout = process.stdout;

console.log = function (d) {
  log_file.write(util.format(d) + "\n\n");
  log_stdout.write(util.format(d) + "\n\n");
};

//end error log

// start the server
app.listen(port, () => {
  console.log(`App Server Listening at ${port}`);
});
