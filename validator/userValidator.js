import joi from "joi";
import error from "../helpers/error";

export const isDataValidate = (req) => {
  try {
    const user = req.body;
    console.log("come here", user);
    if (user) {
      const schema = joi.object({
        firstName: joi
          .string()
          .min(3)
          .max(20)
          .required()
          .error(new Error("firstName must 3 to 20 characters.")),
        lastName: joi
          .string()
          .min(3)
          .max(20)
          .required()
          .error(new Error("lastName must 3 to 20 characters.")),
        email: joi
          .string()
          .email()
          .allow("")
          .error(new Error("Email must be in valid format.")),
        password: joi.when("type", {
          is: "add",
          then: joi
            .string()
            .min(6)
            .max(12)
            .required()
            .error(new Error("Password must be 6 to 12 characters.")),
        }),
        phoneNumber: joi
          .number()
          .min(1000000000)
          .max(9999999999)
          .required()
          .error(new Error("Contact must be 10 digit integer number.")),
        address: joi
          .string()
          .allow("")
          .min(3)
          .max(65)
          .error(new Error("Address  must be 3 to 65 character.")),
        gender: joi.string(),
        biography: joi
          .string()
          .allow("")
          .min(3)
          .max(65)
          .error(new Error("biography must be 3 to 100 character.")),
      });
      const result = joi.validate(user, schema);
      if (result.error !== null)
        return error(
          result.error.message.toString().replace(/"|\\/g, ""),
          "dataNotValid"
        );
    } else return error("User data can not be null.", "userIsNotNull");
    return null;
  } catch (err) {
    console.log(err);
    return error("Bad request at isDataValidate.", "400");
  }
};
