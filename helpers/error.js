export default (msg, field) => {
  const err = new Error(msg);
  err.msg = msg;
  err.field = field;
  return err;
};

export const CustomError = function (msg) {
  this.message = msg;
};
